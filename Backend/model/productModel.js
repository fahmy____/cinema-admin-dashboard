import mongoose from "mongoose";

const productModelSchema = mongoose.Schema(
  {
    category: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Category",
    },
    name_en: {
      type: String,
      required: true,
    },
    name_ar: {
      type: String,
      required: true,
    },
    description_en: {
      type: String,
      required: true,
    },
    description_ar: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },
    contact_phone: { type: Number, required: true },
    cost: { type: Number, required: true },
    has_location: { type: Boolean, required: true, default: false },
    lat: { type: Number },
    lng: { type: Number },
    status: { type: Boolean, default: true },
    is_deleted: { type: Boolean, default: false },
  },
  {
    timestamps: true,
  }
);
const Product = mongoose.model("Product", productModelSchema);
export default Product;
