import mongoose from "mongoose";

const categorySchema = mongoose.Schema(
  {
    parent_category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
    name_en: {
      type: String,
      required: true,
    },
    name_ar: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },
    children: [{ type: mongoose.Schema.Types.ObjectId, ref: "Category" }],
    category_fields: [
      { type: mongoose.Schema.Types.ObjectId, ref: "Category Field" },
    ],
    allow_image: { type: Boolean, default: false },
    is_multiple_images: { type: Boolean, default: false },
    has_map_location: { type: Boolean, default: false },
    status: { type: Boolean, default: true },
    is_deleted: { type: Boolean, default: false },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  },
  { timestamps: true }
);

categorySchema.methods.saveAsChild = async function (createdCat) {
  this.children.push(createdCat._id);
  this.save();
};
categorySchema.methods.deleteChild = async function (id) {
  this.children.pull(id);
  this.save();
};
categorySchema.pre("remove", async function (next) {
  var category = this;
  try {
    if (category.parent_category) {
      await Category.findByIdAndUpdate(
        category.parent_category,
        {
          $pull: {
            children: category._id,
          },
        },
        { new: true }
      );
    } else {
      await Category.deleteMany({ _id: { $in: [category.children] } });
    }
    next();
  } catch (error) {
    next(error);
  }
});
const Category = mongoose.model("Category", categorySchema);

export default Category;
