import mongoose from "mongoose";

const fieldTypeSchema = mongoose.Schema(
  {
    type: { type: String, required: true },
    has_options: { type: Boolean, required: true },
  },
  {
    timestamps: true,
  }
);
const FieldType = mongoose.model("Field Type", fieldTypeSchema);
export default FieldType;
