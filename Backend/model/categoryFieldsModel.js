import mongoose, { mongo } from "mongoose";

const categoryFieldsModelSchema = mongoose.Schema(
  {
    field_type: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Field Type",
    },
    name_en: {
      type: String,
      required: true,
    },
    name_ar: {
      type: String,
      required: true,
    },
    placeholder_en: {
      type: String,
      required: true,
    },
    placeholder_ar: {
      type: String,
      required: true,
    },
    categories: [{ type: mongoose.Schema.Types.ObjectId, ref: "Category" }],
    options: [{}],
    is_required: { type: Boolean, default: true },
  },
  {
    timestamps: true,
  }
);
const CategoryField = mongoose.model(
  "Category Field",
  categoryFieldsModelSchema
);
export default CategoryField;
