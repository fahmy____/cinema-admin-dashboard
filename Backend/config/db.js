import mongoose from "mongoose";

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    console.log(
      `MongoDB is Connected on:${conn.connection.host}`.blue.underline
    );
  } catch (error) {
    console.error(`Error:${error.message}`.red.bold);
    process.exit(1);
  }
};

export default connectDB;
