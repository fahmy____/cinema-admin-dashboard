import express from "express";
import colors from "colors";
import dotenv from "dotenv";
import connectDB from "./config/db.js";
import { NotFound, errorHandler } from "./middleware/errorHandler.js";
import routes from "./routes.js";
import morgan from "morgan";

const app = express();
dotenv.config();
connectDB();
app.use(express.json());
if (process.env.NODE_ENV == "development") {
  app.use(morgan("dev"));
}
app.use("/api", routes);

app.use(NotFound);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;
app.listen(
  PORT,
  console.log(`Server is on ${process.env.NODE_ENV} running ${PORT}`.yellow)
);
