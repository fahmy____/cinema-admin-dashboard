import express from "express";
import {
  createCategory,
  deleteCategoryById,
  getCategories,
  getCategoryById,
  updateCategoryById,
} from "./controllers/categoryController.js";
import {
  authUser,
  createUser,
  getUserProfile,
  getUsers,
  updateUser,
} from "./controllers/userController.js";
import { adminAccess, Protect } from "./middleware/authMiddleware.js";

const routes = express.Router();

routes.route("/categories").get(getCategories);
routes.route("/categories/:id").get(getCategoryById);
routes.route("/categories").post(createCategory);
routes.route("/categories/:id").put(updateCategoryById);
routes.route("/categories/:id").delete(deleteCategoryById);

routes.route("/users").get(Protect, adminAccess, getUsers);
routes.post("/users/login", authUser);
routes.post("/users", createUser);

routes
  .route("/users/profile")
  .get(Protect, getUserProfile)
  .put(Protect, updateUser);
export default routes;
