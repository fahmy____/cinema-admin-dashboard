import asyncHandler from "express-async-handler";
import User from "../model/userModel.js";

import { generateToken } from "../utils.js";

// @desc Fetch All USERS
// @route GET
// @access public

const getUsers = asyncHandler(async (req, res) => {
  const users = await User.find({});

  res.json(users);
});

// @desc Login
// @route POST
// @access public

const authUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email });
  if (!user) {
    res.status(401);
    throw new Error("User not found");
  } else if (!(await user.matchPassword(password))) {
    res.status(401);
    throw new Error("Incorrect Password");
  } else {
    res.send({
      id: user._id,
      first_name: user.first_name,
      last_name: user.last_name,
      role: user.role,
      token: generateToken(user._id),
    });
  }
});

// @desc Create New User
// @route POST
// @access public

const createUser = asyncHandler(async (req, res) => {
  const { first_name, last_name, email, password } = req.body;

  const userExists = await User.findOne({ email });

  if (userExists) {
    res.status(400);
    throw new Error("User Already exists!");
  }

  const user = await User.create({
    first_name,
    last_name,
    email,
    password,
    role: role || "basic",
  });

  if (user) {
    res.status(201).json({
      id: user._id,
      first_name: user.first_name,
      last_name: user.last_name,
      role: user.role,
      token: generateToken(user._id),
    });
  } else {
    res.status(400);
    throw new Error("Data is invalid");
  }
});

// @desc Update User
// @route PUT
// @access Private

const updateUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  user.first_name = req.body.first_name || user.first_name;
  user.last_name = req.body.last_name || user.last_name;
  user.email = req.body.email || user.email;
  if (req.body.password) {
    user.password = req.body.password;
  }
  const updatedUser = await user.save();

  if (updatedUser) {
    res.json({
      email: updatedUser.email,
      first_name: updatedUser.first_name,
      last_name: updatedUser.last_name,
      role: updatedUser.role,
      token: generateToken(updatedUser._id),
    });
  } else {
    res.status(400);
    throw new Error("Data is invalid");
  }
});

// @desc Profile
// @route GET
// @access Private

const getUserProfile = asyncHandler(async (req, res) => {
  res.send(req.user);
});

export { getUsers, authUser, getUserProfile, createUser, updateUser };
