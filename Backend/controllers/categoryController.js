import asyncHandler from "express-async-handler";
import Category from "../model/categoryModel.js";

// @desc Fetch All Categories
// @route GET
// @access public

const getCategories = asyncHandler(async (req, res) => {
  const pageSize = 5;
  const page = Number(req.query.pageNum) || 1;
  const keyWord = req.query.keyword
    ? {
        name_en: {
          $regex: req.query.keyword,
          $options: "i",
        },
      }
    : {};

  const count = await Category.countDocuments({ ...keyWord });
  const categories = await Category.find({ ...keyWord })
    .limit(pageSize)
    .skip(pageSize * (page - 1));

  res.json({ categories, page, pages: Math.ceil(count / pageSize) });
});

// @desc Fetch single Categories
// @route GET /:id
// @access public

const getCategoryById = asyncHandler(async (req, res) => {
  const category = await Category.findById(req.params.id);

  if (category) {
    res.json(category);
  } else {
    res.status(404);
    throw new Error("Category Not found");
  }
});

// @desc Create New Category
// @route POST
// @access public

const createCategory = asyncHandler(async (req, res) => {
  const { parent_category, name_ar, name_en, image } = req.body;

  const createdCat = await Category.create({
    parent_category,
    name_ar,
    name_en,
    image,
  });

  if (createdCat) {
    if (parent_category) {
      const parentCat = await Category.findById(parent_category);

      if (!parentCat) {
        res.status(400);
        throw new Error("Parent Category doesn't exist !");
      } else {
        await parentCat.saveAsChild(createdCat);
      }
    }
    res.status(201).json(createdCat._doc);
  } else {
    res.status(400);
    throw new Error("Data is invalid");
  }
});

// @desc Update Category
// @route PUT
// @access Private

const updateCategoryById = asyncHandler(async (req, res) => {
  const cat = await Category.findById(req.params.id);

  cat.name_en = req.body.name_en || cat.name_en;
  cat.name_ar = req.body.name_ar || cat.name_ar;
  cat.image = req.body.image || cat.image;

  const oldParentId = cat.parent_category;
  const updatedCat = await cat.save();

  if (updatedCat) {
    if (req.body.parent_category) {
      const newParentCat = await Category.findById(req.body.parent_category);
      const oldParentCat = await Category.findById(oldParentId);

      if (!newParentCat || !oldParentCat) {
        res.status(400);
        throw new Error("Parent Category doesn't exist !");
      } else {
        cat.parent_category = req.body.parent_category;
        await newParentCat.saveAsChild(updatedCat);
        await oldParentCat.deleteChild(req.params.id);
      }
    }
    res.status(201).json(updatedCat._doc);
  } else {
    res.status(400);
    throw new Error("Data is invalid");
  }
});

// @desc Delete single Categories
// @route Delete /:id
// @access public

const deleteCategoryById = asyncHandler(async (req, res) => {
  const category = await Category.findById(req.params.id);
  if (category) {
    await category.remove();
    res.status(202).json("Document Deleted Successfully!");
  } else {
    res.status(400);
    throw new Error("Data is invalid");
  }
});

export {
  getCategories,
  getCategoryById,
  createCategory,
  updateCategoryById,
  deleteCategoryById,
};
