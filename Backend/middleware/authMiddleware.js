import asyncHandler from "express-async-handler";
import jwt from "jsonwebtoken";
import User from "../model/userModel.js";

const Protect = asyncHandler(async (req, res, next) => {
  let token = req.headers.authorization;
  if (token && token.startsWith("Bearer")) {
    let tokenCode = req.headers.authorization.split(" ")[1];

    try {
      const decoded = jwt.verify(tokenCode, process.env.JWT_SECRET);
      req.user = await User.findById(decoded.id).select("-password");
      next();
    } catch (error) {
      res.status(401);
      throw new Error("Access Denied!");
    }
  }
  if (!token) {
    res.status(401);
    throw new Error("Access Denied!");
  }
});

const adminAccess = asyncHandler(async (req, res, next) => {
  if (
    req.user &&
    ["admin", "supervisor"].some((substring) =>
      req.user.role.includes(substring)
    )
  ) {
    next();
  } else {
    res.status(401);
    throw new Error("Access Denied! NOT AN ADMIN");
  }
});

export { Protect, adminAccess };
