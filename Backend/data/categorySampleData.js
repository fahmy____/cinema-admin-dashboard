const CategoryData = [
  {
    name_en: "Locations",
    name_ar: "اماكن",
    image: "location.jpg",
  },
  {
    name_en: "Offices & Equipment Suppliers",
    name_ar: "مكاتب و معدات",
    image: "offices.jpg",
  },
  {
    name_en: "Castings/models",
    name_ar: "تمثيل",
    image: "models.jpg",
  },
  {
    name_en: "Technicians",
    name_ar: "فنيين",
    image: "technicians.jpg",
  },
];

export default CategoryData;
