import bcrypt from "bcryptjs";

const UsersData = [
  {
    first_name: "Admin",
    last_name: "User",
    email: "admin@example.com",
    password: bcrypt.hashSync("admin1"),
    role: "admin",
  },
  {
    first_name: "John",
    last_name: "Doe",
    email: "john@example.com",
    password: bcrypt.hashSync("123456"),
  },
  {
    first_name: "Jane",
    last_name: "Doe",
    email: "jane@example.com",
    password: bcrypt.hashSync("123456"),
  },
];

export default UsersData;
