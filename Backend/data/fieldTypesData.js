const FieldTypes = [
  {
    type: "input",
    has_options: false,
  },
  {
    type: "textarea",
    has_options: false,
  },
  {
    type: "dropdown",
    has_options: true,
  },
  {
    type: "checkbox",
    has_options: true,
  },
  {
    type: "radio",
    has_options: true,
  },
];
export default FieldTypes;
