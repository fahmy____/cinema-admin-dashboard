import colors from "colors";
import User from "../model/userModel.js";
import Category from "../model/categoryModel.js";
import FieldType from "../model/fieldTypeModel.js";
import connectDB from "../config/db.js";

import dotenv from "dotenv";
import CategoryData from "./categorySampleData.js";
import UsersData from "./userSampleData.js";
import FieldTypes from "./fieldTypesData.js";

dotenv.config();

connectDB();

const importData = async () => {
  try {
    await User.deleteMany();
    await Category.deleteMany();
    await FieldType.deleteMany();

    FieldType.insertMany(FieldTypes);
    const createdUsers = await User.insertMany(UsersData);
    const userAdmin = createdUsers[0]._id;
    const sampleCategories = CategoryData.map((cat) => {
      return { ...cat, created_by: userAdmin };
    });
    await Category.insertMany(sampleCategories);

    console.log(`Data Imported!`.green.inverse);
    process.exit();
  } catch (error) {
    console.error(`Error:${error}`.red.inverse);
    process.exit(1);
  }
};

const destroyData = async () => {
  try {
    // await User.deleteMany();
    await Category.deleteMany();

    console.log(`Data Destroyed!`.green.inverse);
    process.exit();
  } catch (error) {
    console.error(`Error:${error}`.red.inverse);
    process.exit(1);
  }
};

if (process.argv[2] == "-d") {
  destroyData();
} else {
  importData();
}
