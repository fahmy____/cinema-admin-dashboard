import React from "react";
import {
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
} from "reactstrap";

import * as Icon from "react-feather";

import { history } from "../../../history";
import { useDispatch, useSelector } from "react-redux";
import { LogoutUser } from "../../../redux/actions/userActions";

const NavbarUser = (props) => {
  const dispatch = useDispatch();
  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const logoutHandler = () => {
    dispatch(LogoutUser());
    history.push("/pages/login");
  };

  return (
    <ul className="nav navbar-nav navbar-nav-user float-right">
      <UncontrolledDropdown tag="li" className="dropdown-user nav-item">
        <DropdownToggle tag="a" className="nav-link dropdown-user-link">
          <div className="user-nav d-sm-flex d-none">
            <span className="user-name text-bold-600">
              {userInfo.first_name + " " + userInfo.last_name}
            </span>
            <span className="user-status">{userInfo.role}</span>
          </div>
          <span data-tour="user">
            <img
              src={props.userImg}
              className="round"
              height="40"
              width="40"
              alt="avatar"
            />
          </span>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem tag="a" href="#" onClick={logoutHandler}>
            <Icon.Power size={14} className="mr-50" />
            <span className="align-middle">Log Out</span>
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    </ul>
  );
};

export default NavbarUser;
