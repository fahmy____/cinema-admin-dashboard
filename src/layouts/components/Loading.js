import React from "react";
import { Spinner } from "reactstrap";

export default function Loading() {
  return (
    <div className="text-center">
      <Spinner color="primary" size="lg" />
    </div>
  );
}
