import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { combineReducers } from "redux";

import {
  categoryDeleteReducer,
  categoryListReducer,
  categoryUpdateReducer,
} from "./reducers/categoryReducer";
import customizerReducer from "./reducers/customizer/index";
import navbarReducer from "./reducers/navbar/Index";
import { LoginReducer, UserListReducer } from "./reducers/userReducer";

const rootReducer = combineReducers({
  customizer: customizerReducer,
  userLogin: LoginReducer,
  userList: UserListReducer,
  navbar: navbarReducer,
  categoryList: categoryListReducer,
  categoryDelete: categoryDeleteReducer,
  categoryUpdate: categoryUpdateReducer,
});

const userInfoFromStorage = localStorage.getItem("userInfo")
  ? JSON.parse(localStorage.getItem("userInfo"))
  : null;
const middleware = [thunk];
const initialState = { userLogin: { userInfo: userInfoFromStorage } };
const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export { store };
