import axios from "axios";

export const ListCategories = (keyword = "", pageNum = "") => async (
  dispatch
) => {
  try {
    dispatch({ type: "CATEGORY_LIST_REQUEST" });
    const { data } = await axios.get(
      `/api/categories?keyword=${keyword}&pageNum=${pageNum}`
    );
    dispatch({ type: "CATEGORY_LIST_SUCCESS", payload: data });
  } catch (error) {
    dispatch({
      type: "CATEGORY_LIST_FAIL",
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};

export const DeleteCategory = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: "CATEGORY_DELETE_REQUEST" });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    await axios.delete(`/api/categories/${id}`, config);
    dispatch({ type: "CATEGORY_DELETE_SUCCESS" });
  } catch (error) {
    dispatch({
      type: "CATEGORY_DELETE_FAIL",
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};

export const UpdateCategory = (user) => async (dispatch, getState) => {
  try {
    dispatch({ type: "CATEGORY_UPDATE_REQUEST" });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    await axios.put(`/api/categories/${user._id}`, user, config);
    dispatch({ type: "CATEGORY_UPDATE_SUCCESS" });
  } catch (error) {
    dispatch({
      type: "CATEGORY_UPDATE_FAIL",
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
