export const categoryListReducer = (state = { categories: [] }, action) => {
  switch (action.type) {
    case "CATEGORY_LIST_REQUEST":
      return { loading: true, categories: [] };
    case "CATEGORY_LIST_SUCCESS":
      return {
        loading: false,
        categories: action.payload.categories,
        page: action.payload.page,
        pages: action.payload.pages,
      };
    case "CATEGORY_LIST_FAIL":
      return { loading: false, error: action.payload };

    default:
      return state;
  }
};

export const categoryDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case "CATEGORY_DELETE_REQUEST":
      return { loading: true };
    case "CATEGORY_DELETE_SUCCESS":
      return {
        loading: false,
        success: true,
      };
    case "CATEGORY_DELETE_FAIL":
      return { loading: false, error: action.payload };

    default:
      return state;
  }
};

export const categoryUpdateReducer = (state = {}, action) => {
  switch (action.type) {
    case "CATEGORY_UPDATE_REQUEST":
      return { loading: true };
    case "CATEGORY_UPDATE_SUCCESS":
      return {
        loading: false,
        success: true,
      };
    case "CATEGORY_UPDATE_FAIL":
      return { loading: false, error: action.payload };

    default:
      return state;
  }
};
