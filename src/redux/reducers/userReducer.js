export const LoginReducer = (state = {}, action) => {
  switch (action.type) {
    case "USER_LOGIN_REQUEST":
      return { loading: true };
    case "USER_LOGIN_SUCCESS":
      return { loading: false, userInfo: action.payload };
    case "USER_LOGIN_FAIL":
      return { loading: false, error: action.payload };
    case "USER_LOGOUT":
      return {};
    default:
      return state;
  }
};

export const UserListReducer = (state = { users: [] }, action) => {
  switch (action.type) {
    case "USER_LIST_REQUEST":
      return { loading: true, users: [] };
    case "USER_LIST_SUCCESS":
      return { loading: false, users: action.payload };
    case "USER_LIST_FAIL":
      return { loading: false, error: action.payload };

    default:
      return state;
  }
};
