import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import Message from "../../layouts/components/Message";
import Loading from "../../layouts/components/Loading";
import { ListUsers } from "../../redux/actions/userActions";

export default function UserList() {
  const dispatch = useDispatch();

  const listUsers = useSelector((state) => state.userList);
  const { loading, error, users } = listUsers;

  useEffect(() => {
    dispatch(ListUsers());
  }, [dispatch]);

  return (
    <div>
      <div className="font-large-1">List of Users</div>
      {loading ? (
        <Loading />
      ) : error ? (
        <Message color="danger">{error}</Message>
      ) : (
        users.map((user) => <h5>{user.first_name}</h5>)
      )}
    </div>
  );
}
