import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ListCategories } from "../../redux/actions/categoryAction";
import Message from "../../layouts/components/Message";
import Loading from "../../layouts/components/Loading";
import DataTable from "react-data-table-component";
import { Edit, Trash, ChevronLeft, ChevronRight } from "react-feather";
import ReactPaginate from "react-paginate";
import "../../assets/scss/plugins/extensions/react-paginate.scss";
import "../../assets/scss/pages/data-list.scss";
import { useHistory } from "react-router-dom";

const CategoryList = ({}) => {
  const selectedStyle = {
    rows: {
      selectedHighlighStyle: {
        backgroundColor: "rgba(115,103,240,.05)",
        color: "#7367F0 !important",
        boxShadow: "0 0 1px 0 #7367F0 !important",
        "&:hover": {
          transform: "translateY(0px) !important",
        },
      },
    },
  };

  const columns = [
    {
      name: "Name",
      selector: "name_en",
      minWidth: "300px",
      cell: (row) => (
        <p title={row.name_en} className="text-truncate text-bold-500 mb-0">
          {row.name_en}
        </p>
      ),
    },
    {
      name: "Actions",
      cell: (row) => (
        <div className="data-list-action">
          <Edit
            className="cursor-pointer mr-1"
            size={20}
            onClick={() => console.log("here")}
          />
          <Trash
            className="cursor-pointer"
            size={20}
            onClick={() => console.log("here")}
          />
        </div>
      ),
    },
  ];

  const dispatch = useDispatch();
  const history = useHistory();
  const listCategories = useSelector((state) => state.categoryList);
  const { loading, error, categories, page, pages } = listCategories;

  const handlePagination = (page) => {
    history.push({
      pathname: "/category",
      state: {
        pageNum: page.selected + 1,
      },
    });

    console.log("pageNum:" + history.location.state.pageNum);
  };
  const keyword = "";
  const pageNum = history.location.state ? history.location.state.pageNum : 1;
  useEffect(() => {
    dispatch(ListCategories(keyword, pageNum));
  }, [dispatch, keyword, pageNum]);

  return (
    <React.Fragment>
      {loading ? (
        <Loading />
      ) : error ? (
        <Message color="danger">{error}</Message>
      ) : (
        <div className={"data-list list-view"}>
          <DataTable
            columns={columns}
            data={categories}
            noHeader
            subHeader
            responsive
            pointerOnHover
            selectableRowsHighlight
            customStyles={selectedStyle}
            pagination
            paginationComponent={() => (
              <ReactPaginate
                previousLabel={<ChevronLeft size={15} />}
                nextLabel={<ChevronRight size={15} />}
                breakLabel="..."
                breakClassName="break-me"
                pageCount={pages}
                forcePage={page - 1}
                containerClassName="vx-pagination separated-pagination pagination-end pagination-sm mb-0 mt-2"
                activeClassName="active"
                onPageChange={(page) => handlePagination(page)}
              />
            )}
          />
        </div>
      )}
    </React.Fragment>
  );
};

export default CategoryList;
