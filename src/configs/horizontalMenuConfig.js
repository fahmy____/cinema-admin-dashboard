import React from "react";
import * as Icon from "react-feather";

const horizontalMenuConfig = [
  {
    id: "home",
    title: "Home",
    type: "item",
    icon: <Icon.Home size={20} />,
    permission: [],
    navLink: "/",
  },
  {
    id: "category",
    title: "Category",
    type: "item",
    icon: <Icon.File size={20} />,
    permission: [],
    navLink: "/category",
  },
  {
    id: "user",
    title: "User",
    type: "item",
    icon: <Icon.File size={20} />,
    permission: [],
    navLink: "/user",
  },
];

export default horizontalMenuConfig;
